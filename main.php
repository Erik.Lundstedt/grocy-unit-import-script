#!/usr/bin/env php
<?php
include_once("./secrets.php");
$quantity_units = [
    "Cup" => [
        "name" => "Cup",
        "description" => "",
        "name_plural" => "Cups"
    ],
    "Gallon" => [
        "name" => "Gallon",
        "description" => "",
        "name_plural" => "Gallons"
    ],
    "Gram" => [
        "name" => "Gram",
        "description" => "",
        "name_plural" => "Grams"
    ],
    "Kilogram" => [
        "name" => "Kilogram",
        "description" => "",
        "name_plural" => "Kilograms"
    ],
    "Liter" => [
        "name" => "Liter",
        "description" => "",
        "name_plural" => "Liters"
    ],
    "Milliliter" => [
        "name" => "Milliliter",
        "description" => "",
        "name_plural" => "Milliliters"
    ],
    "Ounce" => [
        "name" => "Ounce",
        "description" => "",
        "name_plural" => "Ounces"
    ],
    "Ounce liquid" => [
        "name" => "Ounce liquid",
        "description" => "",
        "name_plural" => "Ounces liquid"
    ],
    "Pint" => [
        "name" => "Pint",
        "description" => "",
        "name_plural" => "Pints"
    ],
    "Pound" => [
        "name" => "Pound",
        "description" => "",
        "name_plural" => "Pounds"
    ],
    "Quart" => [
        "name" => "Quart",
        "description" => "",
        "name_plural" => "Quarts"
    ],
    "Tablespoon" => [
        "name" => "Tablespoon",
        "description" => "",
        "name_plural" => "Tablespoons"
    ],
    "Teaspoon" => [
        "name" => "Teaspoon",
        "description" => "",
        "name_plural" => "Teaspoons"
    ],
];

$quantity_unit_conversions = [
    "Cup" => [
        "Gallon" => [
            "factor"=> ".0078",
        ],
        "Liter" => [
            "factor"=> "4.1667",
        ],
        "Milliliter" => [
            "factor"=> ".0042",
        ],
        "Ounce liquid" => [
            "factor"=> "8",
        ],
        "Pint" => [
            "factor"=> ".5",
        ],
        "Quart" => [
            "factor"=> ".125",
        ],
        "Tablespoon" => [
            "factor"=> "16",
        ],
        "Teaspoon" => [
            "factor"=> "48",
        ],
    ],
    "Gallon" => [
        "Cup" => [
            "factor"=> "16",
        ],
        "Liter" => [
            "factor"=> ".2642",
        ],
        "Milliliter" => [
            "factor"=> ".0003",
        ],
        "Ounce liquid" => [
            "factor"=> "128",
        ],
        "Pint" => [
            "factor"=> "8",
        ],
        "Quart" => [
            "factor"=> "4",
        ],
        "Tablespoon" => [
            "factor"=> "256",
        ],
        "Teaspoon" => [
            "factor"=> "768",
        ],
    ],
    "Gram" => [
        "Kilogram" => [
            "factor"=> "0.0010",
        ],
        "Ounce" => [
            "factor"=> "28.3495",
        ],
        "Pound" => [
            "factor"=> "453.5920",
        ],
    ],
    "Kilogram" => [
        "Gram" => [
            "factor"=> "1000.0000",
        ],
        "Ounce" => [
            "factor"=> "28349.5000",
        ],
        "Pound" => [
            "factor"=> "453592.0000",
        ],
    ],
    "Liter" => [
        "Cup" => [
            "factor"=> "0.2400",
        ],
        "Gallon" => [
            "factor"=> "3.7850",
        ],
        "Milliliter" => [
            "factor"=> "0.0010",
        ],
        "Ounce liquid" => [
            "factor"=> "0.0296",
        ],
        "Pint" => [
            "factor"=> "0.4730",
        ],
        "Quart" => [
            "factor"=> "1.0570",
        ],
        "Tablespoon" => [
            "factor"=> "0.0148",
        ],
        "Teaspoon" => [
            "factor"=> "0.0049",
        ],
    ],
    "Milliliter" => [
        "Cup" => [
            "factor"=> "240.0000",
        ],
        "Gallon" => [
            "factor"=> "3785.0000",
        ],
        "Liter" => [
            "factor"=> "1000.0000",
        ],
        "Ounce liquid" => [
            "factor"=> "29.5740",
        ],
        "Pint" => [
            "factor"=> "473.0000",
        ],
        "Quart" => [
            "factor"=> "1057.0000",
        ],
        "Tablespoon" => [
            "factor"=> "14.7870",
        ],
        "Teaspoon" => [
            "factor"=> "4.9290",
        ],
    ],
    "Ounce" => [
        "Gram" => [
            "factor"=> "0.0353",
        ],
        "Kilogram" => [
            "factor"=> "0.0000",
        ],
        "Pound" => [
            "factor"=> "0.0625",
        ],
    ],
    "Ounce liquid" => [
        "Cup" => [
            "factor"=> "0.1250",
        ],
        "Gallon" => [
            "factor"=> "0.0078",
        ],
        "Liter" => [
            "factor"=> "33.8135",
        ],
        "Milliliter" => [
            "factor"=> "0.0338",
        ],
        "Pint" => [
            "factor"=> "0.0625",
        ],
        "Quart" => [
            "factor"=> "0.0313",
        ],
        "Tablespoon" => [
            "factor"=> "2.0000",
        ],
        "Teaspoon" => [
            "factor"=> "6.0000",
        ],
    ],
    "Pint" => [
        "Cup" => [
            "factor"=> "2.0000",
        ],
        "Gallon" => [
            "factor"=> "0.1250",
        ],
        "Liter" => [
            "factor"=> "2.1142",
        ],
        "Milliliter" => [
            "factor"=> "0.0021",
        ],
        "Ounce liquid" => [
            "factor"=> "16.0000",
        ],
        "Quart" => [
            "factor"=> "0.5000",
        ],
        "Tablespoon" => [
            "factor"=> "32.0000",
        ],
        "Teaspoon" => [
            "factor"=> "96.0000",
        ],
    ],
    "Pound" => [
        "Gram" => [
            "factor"=> "0.0022",
        ],
        "Kilogram" => [
            "factor"=> "0.0000",
        ],
        "Ounce" => [
            "factor"=> "16.0000",
        ],
    ],
    "Quart" => [
        "Cup" => [
            "factor"=> "4.0000",
        ],
        "Gallon" => [
            "factor"=> "0.2500",
        ],
        "Liter" => [
            "factor"=> "0.9461",
        ],
        "Milliliter" => [
            "factor"=> "0.0009",
        ],
        "Ounce liquid" => [
            "factor"=> "32.0000",
        ],
        "Pint" => [
            "factor"=> "2.0000",
        ],
        "Tablespoon" => [
            "factor"=> "64.0000",
        ],
        "Teaspoon" => [
            "factor"=> "192.0000",
        ],
    ],
    "Tablespoon" => [
        "Cup" => [
            "factor"=> "0.0625",
        ],
        "Gallon" => [
            "factor"=> "0.0039",
        ],
        "Liter" => [
            "factor"=> "67.6270",
        ],
        "Milliliter" => [
            "factor"=> "0.0676",
        ],
        "Ounce liquid" => [
            "factor"=> "0.5000",
        ],
        "Pint" => [
            "factor"=> "0.0313",
        ],
        "Quart" => [
            "factor"=> "0.0156",
        ],
        "Teaspoon" => [
            "factor"=> "3.0000",
        ],
    ],
    "Teaspoon" => [
        "Cup" => [
            "factor"=> "0.0208",
        ],
        "Gallon" => [
            "factor"=> "0.0013",
        ],
        "Liter" => [
            "factor"=> "202.8809",
        ],
        "Milliliter" => [
            "factor"=> "0.2029",
        ],
        "Ounce liquid" => [
            "factor"=> "0.1667",
        ],
        "Pint" => [
            "factor"=> "0.0104",
        ],
        "Quart" => [
            "factor"=> "0.0052",
        ],
        "Tablespoon" => [
            "factor"=> "0.3333",
        ],
    ],
];

function curl_post($stub,$apiaccess,$data)
{
    $options = array(
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_URL => $apiaccess["url"].$stub,
        CURLOPT_FRESH_CONNECT => 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_FORBID_REUSE => 1,
        CURLOPT_TIMEOUT => 4,
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => ["GROCY-API-KEY:".$apiaccess["key"],"Accept: application/json","Content-Type: application/json"],
    );
    $ch = curl_init();
    curl_setopt_array($ch, ($options));
    if( ! $result = curl_exec($ch))
    {
        trigger_error(curl_error($ch));
    }
    curl_close($ch);
    return json_decode($result,true);
}

function curl_get($stub,$apiaccess)
{   
    $options = array(
        CURLOPT_URL => $apiaccess["url"].$stub,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_TIMEOUT => 4,
        CURLOPT_HTTPHEADER => ["GROCY-API-KEY:".$apiaccess["key"],"Accept: application/json"],
    );
   
    $ch = curl_init();
    curl_setopt_array($ch, ($options));
    if( ! $result = curl_exec($ch))
    {
        trigger_error(curl_error($ch));
    }
    curl_close($ch);
    return json_decode($result,true);
}

$currentUnits = curl_get("/objects/quantity_units",$apiaccess);

foreach ($currentUnits as $unit) {
    if (isset($quantity_units[$unit["name"]])) {
        $quantity_units[$unit["name"]]["id"] = $unit["id"];
    }
}

foreach ($quantity_units as $unit => $data) {
    if (!isset($data["id"])){
        $result = curl_post("/objects/quantity_units",$apiaccess,json_encode($data));
        if (isset($result["error_message"])) {
            die($result["error_message"]);
        }
        $quantity_units[$unit]["id"] = $result["created_object_id"];
    }
}

$currentConversions = curl_get("/objects/quantity_unit_conversions",$apiaccess);

foreach ($quantity_unit_conversions as $base => $baseData) {
    $fromId = $quantity_units[$base]["id"];
    $existingConversions = array_filter($currentConversions, function ($var) use ($fromId) {
        return ($var["from_qu_id"] == $fromId);
    });
    $existingConversions = array_column($existingConversions,"to_qu_id");
    foreach ($baseData as $to => $toData) {
        if(!in_array($quantity_units[$to]["id"],$existingConversions)){
            $result = curl_post("/objects/quantity_unit_conversions",$apiaccess,json_encode([
                "from_qu_id" => $fromId,
                "to_qu_id" => $quantity_units[$to]["id"],
                "factor" => $toData["factor"],
            ]));
            if (isset($result["error_message"])) {
                die($result["error_message"]);
            }
        }
    }
}
